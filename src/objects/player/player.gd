extends KinematicBody2D

var velocity = Vector2.ZERO
var speed = 100
var dir_x = 0
var dir_y = 0
var attacking = false
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	get_input()
	move(delta)
	animate()
	keep_on_screen()
	
func get_input():
	dir_x = Input.get_action_strength("player_right") - Input.get_action_strength("player_left")
	dir_y = Input.get_action_strength("player_down") - Input.get_action_strength("player_up")
	
	if Input.is_action_just_pressed("player_attack"):
		attacking = true
		
func stop_attack():
	attacking = false
	
func animate():
	if attacking:
		$AnimationPlayer.play("attack")
	elif velocity.x > 0:
		$AnimationPlayer.play("fly_forward")
	elif velocity.y < 0:
		$AnimationPlayer.play("fly_up")
	else:
		$AnimationPlayer.play("default")
		
func move(delta):
	velocity.x = dir_x * speed * delta * 100
	velocity.y = dir_y * speed * delta * 100
	velocity = move_and_slide(velocity)
	
func keep_on_screen():
	var view := get_viewport_rect()
	position.x = clamp(position.x,0, view.size.x)
	position.y = clamp(position.y,0, view.size.y)
