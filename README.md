# Superman-Godot

# The Video Game Code:
Copyright Kris Occhipinti 2022-07-08

(https://filmsbykris.com)

License GPLv3

# Superman IP and Art
So the art for this game is taken from the Superman Arcade game.
I have no permission to use it or share it.
Feel free to use the code from this project, but art and other assets should not be used.
I mean, I don't care.  But DC Comics probably cares.
